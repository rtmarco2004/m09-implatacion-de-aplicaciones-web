<?php

echo "<table>";
    
    echo "<tr class='hola'>";
        echo "<th id='hola'>Angle</th>";
        echo "<th id='hola'>Radian</th>";
        echo "<th id='hola'>Sinus</th>";
        echo "<th id='hola'>Cosinus</th>";
    
    echo "</tr>";
for ($angle = 0; $angle <= 360; $angle++) {
    $radians = round(deg2rad($angle),4 );
    $sinus = round(sin($radians), 4);
    $cosinus = round(cos($radians), 4);
    
    echo "<tr>";
    echo '<td>' . $angle . '</td>'; 
    echo '<td>' . $radians . '</td>';    

    if ($sinus < 0){
        echo '<td class="negativo">' . $sinus . '</td>'; 
    } else {
        echo '<td class="positivo">' . $sinus . '</td>';
    }
    if ($cosinus < 0){
        echo '<td class="negativo">' . $cosinus . '</td>';
    } else {
        echo '<td class="positivo">' . $cosinus . '</td>';
    }
    echo "</tr>";
}

echo "</table>";