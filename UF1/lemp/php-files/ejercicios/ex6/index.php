<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exponent Calculator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async
src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
</head>
<body class="hola">
<?php include 'ex6.php'; ?>
<div class="buenas">
<h1>Exponent Calculator</h1>

    <div class="prueba">   
            <form class="buenas" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <label for="base">Base:</label><br>
                <input type="text" id="base" name="base" value="<?php echo isset($_POST['base']) ? $_POST['base'] : ''; ?>" required><br>
                <label for="exponent">Exponent:</label><br>
                <input type="text" id="exponent" name="exponent" value="<?php echo isset($_POST['exponent']) ? $_POST['exponent'] : ''; ?>" required><br><br>
                <button type="submit">Calcular Potència</button>
            </form>
            <?php  
            echo "<div class='buenas'>";
            echo numcalculat(); // Esta función genera el texto debajo del formulario
            echo "</div>";
            ?>
    </div>
   <br>
    <h2>Positive exponent</h2>
    <p>Exponentiation is a mathematical operation, written as \(x^n\), involving the base x and an exponent n. In the case where n is a positive integer, exponentiation corresponds torepeated multiplication of the base, n times.</p>
    <br>
    <p>
        \[x^n = x · x·....·x\] 
        \[n \space times \]

    </p>
    <p>The calculator above accepts negative bases, but does not compute imaginary numbers. It also does not accept fractions, but can be used to compute fractional exponents, as long as the exponents are input in their decimal form.</p>
    <p>
        \[x^n = {1 \over x·x·....·x}\]
    </p>
    <h2>Negative exponent</h2><br>
    <p>When an exponent is negative, the negative sign is removed by reciprocating the base and raising it to the positive exponent.</p>
    <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recursesuntil n is equal to 0. </p>
    <h2>Exponent 0</h2>
    <p>When an exponent is 0, any number raised to the 0 power is 1.</p>
    <p>
        \[x^n = 1\]
    </p>
    <p>For 0 raised to the 0 power the answer is 1 however this is considered a definition and not an actual calculation</p>
    <h2>Real number exponent</h2>
    <p>For real numbers, we use the PHP function pow(n,x). </p>
</div>
</body>

