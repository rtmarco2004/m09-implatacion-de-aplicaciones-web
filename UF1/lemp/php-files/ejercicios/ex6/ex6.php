<?php
function numcalculat() {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $base = $_POST['base'];
        $exponent = $_POST['exponent'];
        if (is_numeric($base) && is_numeric($exponent)) {
            if ($exponent != intval($exponent)) {
                $result = pow($base, $exponent);
                echo "<p>La potència de $base elevat a $exponent és: $result</p>";
            } else {
                $result = 1;
                if ($exponent > 0) {
                    for ($i = 0; $i < $exponent; $i++) {
                        $result *= $base;
                    }
                } elseif ($exponent < 0) {
                    $exponent = abs($exponent);
                    for ($i = 0; $i < $exponent; $i++) {
                        $result *= (1 / $base);
                    }
                }
                echo "<p>La potència de $base elevat a $exponent és: $result</p>";
            }
        } else {
            echo "<p style='color: red;'>Si us plau, introdueix números vàlids.</p>";
        }
    }
}
?>
