<?php
$len = 20;
$len2 = 20;
$randomArray = array();

echo "<h1>Tabla 1</h1>";
echo "<table class='table table-bordered' table border='1'>";
echo "<tr>";
for ($i = 0; $i < $len; $i++) {
    $randomArray[$i] = rand(100, 999);
    
}
echo "</tr>";
echo "<tr>";

for ($i=0; $i < $len ; $i++) { 
    echo "<td>$i</td>";
}
echo "</tr>";
echo "<tr>";
for ($i=0; $i < $len ; $i++) { 
    echo "<td>$randomArray[$i]</td>";
}
echo "</tr>";
echo "</table>";


$len2 = 20; // Longitud del array original

$par = array();
$impar = array();
$sumapares = 0;
$sumaimpares = 0;

echo "<br>";
echo "<br>";
echo "<h1>Tabla 2 Parells/Imparells</h1>";

echo "<table class='table table table w-auto' border='1'>";
echo "<tr>";

for ($i = 0; $i < $len; $i++) {
    echo "<td>$i</td>";
}

echo "</tr>";
echo "<tr>";

for ($i = 0; $i < $len; $i++) {
    if ($randomArray[$i] % 2 == 0) {
        echo "<td>$randomArray[$i]</td>";
        $par[] = $randomArray[$i];
        $sumapares += $randomArray[$i];
    }
}

for ($i = 0; $i < $len; $i++) {
    if ($randomArray[$i] % 2 != 0) {
        echo "<td>$randomArray[$i]</td>";
        $impar[] = $randomArray[$i];
        $sumaimpares += $randomArray[$i];
    }
}

echo "</tr>";
$sumatotal = $sumaimpares + $sumapares;
$mitjana = $sumatotal / 20;
echo "</table>";
echo "<br>";

echo "<h1>Sumes de Arrays</h1>";
echo "Total suma parells: ";
echo "<br>";
echo $sumapares;
echo "<br>";
echo "<br>";
echo "Total suma imparells: ";
echo "<br>";
echo $sumaimpares;
echo "<br>";
echo "<br>";

echo "Total: ";
echo "<br>";
echo $sumatotal;
echo "<br>";


echo "<h1>Mitjana</h1>";

echo $mitjana;


echo "<h1>Taula Multiples de 10</h1>";

echo "<table class='table table w-auto' border='1'>";


echo "<tr>";


echo "<tr>";

$index = 1;

for ($i = 0; $i < $len; $i++) {
    if ($randomArray[$i] % 10 == 0) {
        echo "<td>" . $index . "</td>";
        $index++;
    }
}

echo "</tr>";

for ($i = 0; $i < $len; $i++) {
    if ($randomArray[$i] % 10 == 0) {
        echo "<td>$randomArray[$i]</td>";
    }
}

echo "</tr>";
echo "</table>";


$mesgran = [];

foreach ($randomArray as $num) {
    if ($num > round($sumatotal / $len)) {
        $mesgran[] = $num;
    }
}

echo "<h1>Taula més grans que la mitjana</h1>";
echo "<table  class='table table w-auto' border='1'>";

echo "<tr>";
$index = 1;
foreach ($mesgran as $num) {
    echo "<td>" . $index . "</td>";
    $index++;
}
echo "</tr>";

echo "<tr>";
foreach ($mesgran as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

echo "</table>";

$mespeque = [];

foreach ($randomArray as $num) {
    if ($num < round($mitjana)) {
        $mespeque[] = $num;
    }
}

echo "<h1>Taula més petita que la mitjana</h1>";
echo "<table  class='table table w-auto' border='1'>";

echo "<tr>";
$index = 1;
foreach ($mespeque as $num) {
    echo "<td>" . $index . "</td>";
    $index++;
}
echo "</tr>";

echo "<tr>";
foreach ($mespeque as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

echo "</table>";

?>
