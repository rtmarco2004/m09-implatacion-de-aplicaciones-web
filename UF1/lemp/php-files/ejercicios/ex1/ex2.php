<?php
$vendes = array(
    array('Nintendo Switch', 60),
    array('Game Boy Advance', 81),
    array('Play Station 4', 102),
    array('Xbox 360', 84),
    array('Nes', 62),
    array('Playstation 2', 1555),
    array('Wii', 101),
    array('Play Station 3', 87),
    array('Play Station Portable', 82),
    array('Nintendo 3DS', 72),
    array('Nintendo DS', 154),
    array('Game Boy', 119)
);

$limite_maximo = 100;


// Ordenar el array de mayor a menor según el número de ventas
usort($vendes, function($a, $b) {
    return $b[1] - $a[1];
});

echo "<table>";
echo "<tr><th>Consola</th><th>Barra</th><th>Vendes</th></tr>";

// Mostrar datos
foreach ($vendes as $consola) {
    echo "<tr>";
    echo "<td>" . $consola[0] . " : " . "</td>";
    $Factor = $consola[1] * 100 / 1555;

    echo "<td>";
    $longitud_barra = min($consola[1] * 2, round($Factor));  // Corregir aquí
    
    echo str_repeat('<img src="green.png" alt="green" >', $longitud_barra);    
    echo "</td>";
    
    echo "<td>" . $consola[1] . ' Millions' . "</td>";
    
    echo "</tr>";
}

echo "</table>";
?>

