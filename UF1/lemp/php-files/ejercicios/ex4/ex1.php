<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora d'IVA</title>
    <style>
        .error {color: #FF0000;}
    </style>
</head>
<body>
    <h1>PRICE, TAX and ROUNDS</h1>
    <?php
    $taxErr = $extraErr = "";
    $tax = $extra = "";

    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["execute"])) {
        if (empty($_GET["tax"]) || empty($_GET["extra"])) {
            echo "<span class='error'>Los campos no pueden estar vacíos.</span><br/><br/>";
        } else {
            $tax = validate($_GET["tax"]);
            $extra = validate($_GET["extra"]);

            if (!is_numeric($tax)) {
                $taxErr = "El preu amb IVA ha de ser un número";
            }

            if (!is_numeric($extra)) {
                $extraErr = "El tant per cent d'IVA ha de ser un número";
            }
        }
    }
    ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="get">
        <label for="tax">Preu amb IVA:</label><br/>
        <input id="tax" type="text" name="tax" value="<?php echo $tax?>" /><span class="error">* <?php echo $taxErr?></span><br/><br/>
        <label for="extra">Tant per cent d'IVA (%):</label><br/>
        <input id="extra" type="text" name="extra" value="<?php echo $extra?>" /><span class="error">* <?php echo $extraErr?></span><br/><br/>
        <input type="submit" name="execute">
    </form>
    <h2>TAX DATA:</h2>
    <?php
    if(isset($_GET["execute"]) && empty($taxErr) && empty($extraErr)) {
        $preu_iva = floatval($tax);
        $iva_percentatge = floatval($extra);
        $preu_sense_iva = $preu_iva / (1 + $iva_percentatge / 100);
        echo "1. Price without tax : " . $preu_sense_iva . "<br/>";
        
        $preu_sense_iva_rounded = round($preu_sense_iva, 2);
        echo "2. Rount to 2 decimals using round() : " . $preu_sense_iva_rounded . "<br/>";
        
        $preu_sense_iva_formatted = sprintf("%.4f", $preu_sense_iva);
        echo "3. Using function sprintf() : " . $preu_sense_iva_formatted . "<br/>";
    }
    ?>
</body>
</html>

