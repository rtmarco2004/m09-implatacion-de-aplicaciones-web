<!DOCTYPE html>
<html lang="ca">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Llançament de Daus</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<div>
    <h1>Llançament de Daus</h1>
    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <label for="number">Nombre de Daus:</label><br>
        <input type="number" id="number" name="number" min="1" required value="<?php echo isset($_POST['number']) ? $_POST['number'] : ''; ?>"><br><br>
        <button type="submit">Llançar Daus</button>
    </form>
    <?php include 'act7.php'; ?>
</div>

</body>
</html>
