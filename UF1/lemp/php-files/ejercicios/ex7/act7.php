<?php
$dados = array (
    array("imagenes/imagen1.png", 1),
    array("imagenes/imagen2.png", 2),
    array("imagenes/imagen3.png", 3),
    array("imagenes/imagen4.png", 4),
    array("imagenes/imagen5.png", 5),
    array("imagenes/imagen6.png", 6)
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $numero = $_POST['number'];
    if (is_numeric($numero) && $numero > 0) {
        $sum = 0;
        $contador = array_fill(1, 6, 0);
        echo "<h2>Resultado:</h2>";
        echo "<p>";
        for ($i = 0; $i < $numero; $i++) {
            $girar = rand(0, 5);
            $imagen = $dados[$girar][0];
            $valor = $dados[$girar][1];
            echo "<img src='$imagen' alt='Dau $valor'>";
            $sum += $valor;
            $contador[$valor]++;
        }
        echo "</p>";
        echo "<h2>Suma Total: $sum</h2>";
        echo "<h3>Nombre total de cada dado:</h3>";
        echo "<ul>";
        foreach ($contador as $value => $count) {
            echo "<li>Número $value: $count dau(s)</li>";
        }
        echo "</ul>";
    } else {
        echo "<p style='color: red;'>Si us plau, introdueix un nombre vàlid de daus.</p>";
    }
}
?>

