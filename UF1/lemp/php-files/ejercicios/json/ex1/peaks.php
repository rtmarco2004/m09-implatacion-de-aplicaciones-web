<?php

$url = "https://api.jsonserve.com/Gk1Go7";
$json = file_get_contents($url);
$data = json_decode($json, true);

if ($data === null) {
    echo "No s'ha pogut llegir el fitxer JSON.";
    exit;
}

echo "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>";
echo "<div class='container'>";
echo "<table class='table'>";
echo "<thead class='thead-dark'>";
echo "<tr><th scope='col' style='text-align: center;'>Nom</th><th scope='col' style='text-align: center;'>Alçada</th><th scope='col' style='text-align: center;'>Prominència</th><th scope='col' style='text-align: center;'>Zona</th><th scope='col' style='text-align: center;'>Imagen</th><th scope='col' style='text-align: center;'>País</th></tr>";
echo "</thead>";
echo "<tbody>";

foreach ($data as $mountain) {
    echo "<tr>";
    echo "<td class='align-middle' style='text-align: center;'>".$mountain['name']."</td>";
    echo "<td class='align-middle'  style='text-align: center;'>".$mountain['height']."</td>";
    echo "<td class='align-middle'  style='text-align: center;'>".$mountain['prominence']."</td>";
    echo "<td class='align-middle' style='text-align: center;'>".$mountain['zone']."</td>";
    echo "<td class='align-middle' style='text-align: center;'><a href='".$mountain['url']."' target='_blank'><img src='".$mountain['url']."' alt='Mountain' class='img-fluid' style='width: 250px;'></a></td>";
    echo "<td class='align-middle' style='text-align: center;'>".$mountain['country']."</td>";
    echo "</tr>";
}

echo "</tbody>";
echo "</table>";
echo "</div>";
echo "<script src='https://code.jquery.com/jquery-3.5.1.slim.min.js'></script>";
echo "<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js'></script>";
echo "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js'></script>";

?>


