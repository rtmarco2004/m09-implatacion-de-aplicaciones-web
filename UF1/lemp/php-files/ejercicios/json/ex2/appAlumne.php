<?php

//Get all countries JSON file
$url = "https://www.thesportsdb.com/api/v1/json/3/all_countries.php"; // path to your JSON file
$json_countries = file_get_contents($url); 
$countries_data = json_decode($json_countries, true);

//Create form
echo "<div style='max-width: 900px; margin: 0 auto;'>";
echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
echo "<div style='margin-bottom: 15px; text-align: center;'>";
echo "<label for='country' style='display: block; font-weight: bold;'>Select Country:</label>";
echo "<select name='country' style='width: 100%; border-radius: 5px; border: 1px solid #ccc;'>";

//Iterating $array
foreach ($countries_data["countries"] as $country) {
    // Check if this option is selected
    $selected = isset($_POST['country']) && $_POST['country'] == $country["name_en"] ? 'selected' : '';
    //Add all elements of array to <option></option>
    echo "<option value='" . $country["name_en"] . "' $selected>" . $country["name_en"] . "</option>";
}

echo "</select>";
echo "</div>";
//Add <input type='submit' name='execute'/>
echo "<div style='text-align: center;'>";
echo "<button type='submit' name='execute' style='background-color: #007bff; color: #fff; border: none; border-radius: 5px; cursor: pointer; padding: 5px 20px;'>Search</button>";
echo "</div>";
echo "</form>";

//if $_SERVER['REQUEST_METHOD'] === 'POST' and isset($_POST['execute']), then call execute($_POST['country']) method
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['execute'])) {
    execute($_POST['country']);
}

function execute($country)
{
    //Get JSON object with json_decode() with associative array
    $url = "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" . urlencode($country); // path to your JSON file
    $json_leagues = file_get_contents($url); 
    $leagues_data = json_decode($json_leagues, true);
    if(isset($leagues_data["countries"])){
    echo "<div style='margin-top: 20px;'>";
    echo "<table style='width: 100%; border-collapse: collapse;'>";
    echo "<thead><tr style='background-color: #007bff; color: #fff;'><th style='border: 1px solid #ccc;'>Flag</th><th style='border: 1px solid #ccc;'>Title</th><th style='border: 1px solid #ccc;'>Website</th><th style='border: 1px solid #ccc;'>Image</th></tr></thead>";
    echo "<tbody>";
  
    //Iterating each object of countries
   
    $rowColor = '#f2f2f2'; // Starting row color
    foreach ($leagues_data["countries"] as $league) {
        // Add flag
        echo "<tr style='border-bottom: 1px solid #ccc; background-color: $rowColor;'>";
        echo "<td style='border: 1px solid #ccc; padding: 15px;'>" . ($league['strBadge'] ? "<img src='".$league['strBadge']."' alt='Flag' style='max-width: 100px; max-height: 100px;'>" : "") . "</td>";
        // Add title
        echo "<td style='border: 1px solid #ccc; padding: 15px;'>" . $league['strLeague'] . "</td>";
        // Add website
        echo "<td style='border: 1px solid #ccc; padding: 15px;'>" . ($league['strWebsite'] ? "<a href='https://" . $league['strWebsite'] . "' target='_blank'>Link web</a>" : "") . "</td>";
        // Add image
        echo "<td style='border: 1px solid #ccc; padding: 15px;'>" . ($league['strFanart1'] ? "<a href='".$league['strFanart1']."' target='_blank'><img src='".$league['strFanart1']."' alt='Image' style='max-width: 150px; max-height: 150px;'></a>" : "") . "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td colspan='4' style='border: 1px solid #ccc; background-color: $rowColor;'>" . $league['strDescriptionEN'] . "</td>";
        echo "</tr>";
        // Switch row color for next row
        $rowColor = $rowColor === '#f2f2f2' ? '#ffffff' : '#f2f2f2';
    }
  
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    } else{
        echo "<p>No existen equipos en este pais ¯\_(ツ)_/¯ prueba con otro 😘</p>";
    }
}

echo "</div>";

?>






