<?php
require_once 'author.php';
require_once 'language.php';
require_once 'library.php';
require_once 'publisher.php';



class Book
{
    private int $book_id;
    private string $title;
    private string $isbn13;
    private Language $language;
    private int $num_pages;
    private string $publication_date;
    private Publisher $publisher;
    private array $authors = [];

    public function __construct(int $book_id, string $title, string $isbn13, Language $language, int $num_pages, string $publication_date, Publisher $publisher)
    {
        $this->book_id = $book_id;
        $this->title = $title;
        $this->isbn13 = $isbn13;
        $this->language = $language;
        $this->num_pages = $num_pages;
        $this->publication_date = $publication_date;
        $this->publisher = $publisher;
    }

    // Getters and setters
    public function getBookId(): int
    {
        return $this->book_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
    public function getIsbn13(): string
    {
        return $this->isbn13;
    }
    public function getPublicationDate(): string
    {
        return $this->publication_date;
    }
    
    public function getNumPages(): string
    {
        return $this->num_pages;
    }
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    // Implement getters and setters for other properties...

    public function addAuthor(Author $author): void
    {
        $this->authors[] = $author;
    }

    public function removeAuthor(Author $author): void
    {
        $this->authors = array_filter($this->authors, function ($a) use ($author) {
            return $a !== $author;
        });
    }

    public function __toString(): string
    {
        $output = "Book ID: " . $this->book_id . "<br>";
        $output .= "Title: " . $this->title . "<br>";
        $output .= "ISBN-13: " . $this->isbn13 . "<br>";
        // Add more properties to the output...
        $output .= "Language: " . $this->language->getLanguageName()() . "<br>";
        $output .= "Publisher: " . $this->publisher->getPublisherName() . "<br>";
        $output .= "Authors: " . implode(", ", array_map(function ($author) {
            return $author->getAuthorName();
        }, $this->authors)) . "<br>";

        return $output;
    }
}
?>
