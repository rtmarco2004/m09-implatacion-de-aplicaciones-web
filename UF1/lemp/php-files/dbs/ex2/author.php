<?php
class Author
{
    // Properties
    private int $author_id;
    private string $author_name;

    // Constructor
    public function __construct(int $author_id, string $author_name)
    {
        $this->author_id = $author_id;
        $this->author_name = $author_name;
    }

    // Getters
    public function getAuthorId(): int
    {
        return $this->author_id;
    }

    public function getAuthorName(): string
    {
        return $this->author_name;
    }

    // Setters
    public function setAuthorId(int $author_id): void
    {
        $this->author_id = $author_id;
    }

    public function setAuthorName(string $author_name): void
    {
        $this->author_name = $author_name;
    }

    // toString
    public function __toString(): string
    {
        return "Author ID: " . $this->author_id . "<br>" .
               "Author Name: " . $this->author_name . "<br>";
    }
}
?>
