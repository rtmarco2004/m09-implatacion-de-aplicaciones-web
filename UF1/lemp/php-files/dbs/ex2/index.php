<!DOCTYPE html>
<html>
<head>
    <title>Buscar libros</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
        body {
            font-family: Arial, sans-serif;
        }
    </style>
</head>
<body>

<h1>Buscar libros</h1>

<form method="GET">
    <label for="search">Buscar:</label>
    <input type="text" id="search" name="search" placeholder="Ingrese una palabra en el título" required>
    <input type="submit" value="Buscar">
</form>

<?php
require_once 'language.php';
require_once 'publisher.php';
require_once 'author.php';
require_once 'library.php';
require_once 'book.php';

$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("<p>Error de conexión a la base de datos: " . $conn->connect_error . "</p>");
}

$search = isset($_GET['search']) ? $_GET['search'] : '';

$sql = "SELECT b.book_id, b.title, bl.language_id, bl.language_name, bl.language_code,
               p.publisher_id, p.publisher_name, a.author_id, a.author_name, b.isbn13,
               b.num_pages, b.publication_date
        FROM book b
        INNER JOIN book_language bl ON b.language_id = bl.language_id
        INNER JOIN publisher p ON b.publisher_id = p.publisher_id
        INNER JOIN book_author ba ON b.book_id = ba.book_id
        INNER JOIN author a ON a.author_id = ba.author_id
        WHERE LOWER(b.title) LIKE LOWER(?)
        ORDER BY b.book_id";

$stmt = $conn->prepare($sql);

$searchParam = '%' . $search . '%';
$stmt->bind_param("s", $searchParam);

$stmt->execute();

$result = $stmt->get_result();

if ($result->num_rows > 0) {
    echo "<table>";
    echo "<tr><th>ID</th><th>Título</th><th>Idioma</th><th>Editorial</th><th>Autor</th><th>ISBN</th><th>Fecha de publicación</th><th>Número de páginas</th></tr>";
    
    $library = new Library("Mi Biblioteca");
    
    while ($row = $result->fetch_assoc()) {
        $language = new Language($row["language_id"], $row["language_code"], $row["language_name"]);
        $publisher = new Publisher($row["publisher_id"], $row["publisher_name"]);
        $author = new Author($row["author_id"], $row["author_name"]);
        
        $book = new Book($row["book_id"], $row["title"], $row["isbn13"], $language, $row["num_pages"], $row["publication_date"], $publisher);
        
        $book->addAuthor($author);
        
        $library->add_book($book);
        
        echo "<tr>";
        echo "<td>" . $book->getBookId() . "</td>";
        echo "<td>" . $book->getTitle() . "</td>";
        echo "<td>" . $language->getLanguageName() . "</td>";
        echo "<td>" . $publisher->getPublisherName() . "</td>";
        echo "<td>" . $author->getAuthorName() . "</td>";
        echo "<td>" . $book->getIsbn13() . "</td>";
        echo "<td>" . date("d/m/Y", strtotime($book->getPublicationDate())) . "</td>";
        echo "<td>" . $book->getNumPages() . "</td>";
        echo "</tr>";
    }

    echo "</table>";
} else {
    echo "<p>No se encontraron libros que coincidan con la búsqueda.</p>";
}

$stmt->close();
$conn->close();
?>

</body>
</html>

