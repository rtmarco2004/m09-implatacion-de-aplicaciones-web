<?php

class Country {
    private $country_id;
    private $country_name;

    public function __construct($country_id, $country_name) {
        $this->country_id = $country_id;
        $this->country_name = $country_name;
    }

    // Getters and setters
    public function getCountryId() {
        return $this->country_id;
    }

    public function setCountryId($country_id) {
        $this->country_id = $country_id;
    }

    public function getCountryName() {
        return $this->country_name;
    }

    public function setCountryName($country_name) {
        $this->country_name = $country_name;
    }
}