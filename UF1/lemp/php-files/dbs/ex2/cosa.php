<?php

class Address {
    private $address_id;
    private $street_number;
    private $street_name;
    private $city;
    private $country;

    public function __construct($address_id, $street_number, $street_name, $city, $country) {
        $this->address_id = $address_id;
        $this->street_number = $street_number;
        $this->street_name = $street_name;
        $this->city = $city;
        $this->country = $country;
    }

    // Getters and setters
    public function getAddressId() {
        return $this->address_id;
    }

    public function setAddressId($address_id) {
        $this->address_id = $address_id;
    }

    public function getStreetNumber() {
        return $this->street_number;
    }

    public function setStreetNumber($street_number) {
        $this->street_number = $street_number;
    }

    public function getStreetName() {
        return $this->street_name;
    }

    public function setStreetName($street_name) {
        $this->street_name = $street_name;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setCountry($country) {
        $this->country = $country;
    }
}