<?php
require_once('user.php');
require_once('status.php');

$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $status_id = $_POST['status_id'];
    $info = $_POST['info'];

    $status = new Status($status_id, '');

    $user = new User(0, $first_name, $last_name, $email, $password, $status, $info);

    $sql = "INSERT INTO users (user_first_name, user_last_name, user_email, user_password, user_status_id, user_info) 
            VALUES ('{$user->getUser_first_name()}', '{$user->getUser_last_name()}', '{$user->getUser_email()}', '{$user->getUser_password()}', '{$user->getStatus()->getStatusId()}', '{$user->getUser_info()}')";

    if ($conn->query($sql) === TRUE) {
        echo "Usuario insertado correctamente.";
    } else {
        echo "Error al insertar usuario: " . $conn->error;
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
    <title>User Insert</title>
</head>
<body>
    <h2>Insert Users</h2>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="first_name">First Name:</label><br>
        <input type="text" id="first_name" name="first_name" required><br>
        <label for="last_name">Last Name:</label><br>
        <input type="text" id="last_name" name="last_name" required><br>
        <label for="email">Email:</label><br>
        <input type="email" id="email" name="email" required><br>
        <label for="password">Password:</label><br>
        <input type="password" id="password" name="password" required><br>
        <label for="status_id">Status:</label><br>
        <select id="status_id" name="status_id" required>
            <option value="1">Activo</option>
            <option value="2">Inactivo</option>
        </select><br>
        <label for="info">Información adicional:</label><br>
        <textarea id="info" name="info" rows="4" cols="50"></textarea><br><br>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>
