<!DOCTYPE html>
<html>
<head>
    <title>Adressess</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<h1>Addresses</h1>

<?php
$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("<p>Error de conexión a la base de datos: " . $conn->connect_error . "</p>");
}
require_once 'address.php';

$sql = "SELECT a.address_id, a.street_number, a.street_name, a.city, a.country_id, c.country_name
        FROM address a
        INNER JOIN country c ON a.country_id = c.country_id
        ORDER BY a.address_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table>";
    echo "<tr><th>Address ID</th><th>Street Number</th><th>Street Name</th><th>City</th><th>Country ID</th><th>Country Name</th></tr>";
    while($row = $result->fetch_assoc()) {
        $country = new Country($row["country_id"], $row["country_name"]);
        $address = new Address($row["address_id"], $row["street_number"], $row["street_name"], $row["city"], $country);
        
        echo "<tr>";
        echo "<td>" . $address->getAddress_id() . "</td>";
        echo "<td>" . $address->getStreet_number() . "</td>";
        echo "<td>" . $address->getStreet_name() . "</td>";
        echo "<td>" . $address->getCity() . "</td>";
        echo "<td>" . $address->getCountry()->getCountryId() . "</td>";
        echo "<td>" . $address->getCountry()->getCountryName() . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "No se encontraron direcciones.";
}

$conn->close(); 
?>

</body>
</html>