<?php

class Status
{
    // Properties
    private int $status_id;
    private string $status_name;
    
    public function __construct($status_id, $status_name) {
        $this->status_id = $status_id;
        $this->status_name = $status_name;
    }


    public function getStatusId() {
        return $this->status_id;
    }

    public function setStatusId($status_id) {
        $this->status_id = $status_id;
    }

    public function getStatusName() {
        return $this->status_name;
    }

    public function setStatusName($status_name) {
        $this->status_name = $status_name;
    }

}
