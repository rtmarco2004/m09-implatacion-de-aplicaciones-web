<?php
require_once('status.php');
class User
{
    // Properties
    private int $user_id;
    private string $user_first_name;
    private string $user_last_name;
    private string $user_email;
    private string $user_password;
    private Status $status;
    private string $user_info;

    public function __construct(int $user_id, string $user_first_name, string $user_last_name,string $user_email ,string $user_password, status $status, string $user_info)
    {
        $this->user_id = $user_id;
        $this->user_first_name = $user_first_name;
        $this->user_last_name = $user_last_name;
        $this->user_email = $user_email;
        $this->user_password = $user_password;
        $this->status = $status;
        $this->user_info = $user_info;
    }

    public function getUser_id(): int
    {
        return $this->user_id;
    }
    public function getUser_first_name(): string
    {
        return $this->user_first_name;
    }
    public function getUser_last_name():string
    {
        return $this->user_last_name;
    }
    public function getUser_email(): string
    {
        return $this->user_email;
    }
    public function getUser_password(): string
    {
        return $this->user_password;
    }
    public function getStatus(): Status
    {
        return $this->status;
    }
    public function getUser_info(): string
    {
        return $this->user_info;
    }
}
