<?php
require_once('country.php');

class Address
{
    // Properties
    private int $address_id;
    private string $street_number;
    private string $street_name;
    private string $city;
    private Country $country;

    public function __construct(int $address_id, string $street_number, string $street_name, string $city, Country $country)
    {
        $this->address_id = $address_id;
        $this->street_number = $street_number;
        $this->street_name = $street_name;
        $this->city = $city;
        $this->country = $country;
    }

    public function getAddress_id(): int
    {
        return $this->address_id;
    }

    public function getStreet_number(): string
    {
        return $this->street_number;
    }

    public function getStreet_name(): string
    {
        return $this->street_name;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }
}
?>