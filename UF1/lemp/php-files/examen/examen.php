<!doctype html>
<html>
<head>
  <title>Examen</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script></head>
<body>

<?php

$url_anime="https://joanseculi.com/edt69/animes3.php";
$json_anime=file_get_contents($url_anime);
$data_anime=json_decode($json_anime,true);

echo "<h1>Animes</h1>";

echo "<table border='1' style=' border-collapse: collapse;'>";
echo "<tr><th>Numero Animes:</th>";
$contador=0;
foreach($data_anime["animes"] as $anime){
    $contador++;
}
echo "<td>$contador</td>";
echo "</tr>";
echo "</table>";

echo "<br>";

echo "<table class='table table-bordered'>";
$anys=[];
foreach ($data_anime['animes'] as $anime) {
    $animeyear = explode(', ', $anime['year']);
    $anys = array_merge($anys, $animeyear);
}
$anys = array_unique($anys);
sort($anys);

echo '<tr>';
echo '<th>Years:</th>';
foreach ($anys as $any) {
    echo "<td>$any</td>";
}

echo "</table>";

echo "<br>";

echo"<table class='table table-bordered'>";

$generes = [];

foreach ($data_anime['animes'] as $anime) {
    $animeGeneres = explode(', ', $anime['genre']);
    $generes = array_merge($generes, $animeGeneres);
}
$genrees = array_unique($generes);
sort($generes);

echo '<tr>';
echo '<th>Genres:</th>';
foreach ($generes as $genre) {
    echo "<td>$genre</td>";
}

echo "</table>";
echo "<br>";

echo "<table border='1'style='width: 100%; border-collapse: collapse;' class='table table-borderless'>";
echo "<tr><th>Image</th><th>ID</th><th>Type</th><th>Name</th><th>Year</th><th>Original name</th><th>Rating</th><th>Demography</th><th>Genre</th><th>Synopsis</th></tr>";

foreach($data_anime["animes"] as $anime){
    echo "<tr>";
    echo "<td><img style= width:200px; src='https://joanseculi.com/{$anime['image']}'</td>";
    echo "<td>{$anime['id']}</td>";
    echo "<td>{$anime['type']}</td>";
    echo "<td>{$anime['name']}</td>";
    echo "<td>{$anime['year']}</td>";
    echo "<td>{$anime['originalname']}</td>";
    echo "<td>{$anime['rating']}</td>";
    echo "<td>{$anime['demography']}</td>";
    echo "<td>{$anime['genre']}</td>";
    echo "<td>{$anime['description']}</td>";
    echo"</tr>";
}
echo "</table>";

