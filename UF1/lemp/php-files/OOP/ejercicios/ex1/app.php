<?php
    include_once("pokemon.php");
    include ("pokemons.php");

    $json_url = 'http://joanseculi.com/json/pokemons.json';
    $json_data = file_get_contents($json_url);
    $pokemons_data = json_decode($json_data, true);
    
$pokemons = new pokemons();

foreach ($pokemons_data["pokemons"] as $pokemon) {

    $pokemon = new Pokemon(
        $pokemon['Code'],
        $pokemon['Name'],
        $pokemon['Type1'],
        $pokemon['Type2'],
        $pokemon['HealthPoints'],
        $pokemon['Attack'],
        $pokemon['Defense'],
        $pokemon['SpecialAttack'],
        $pokemon['SpecialDefense'],
        $pokemon['Speed'],
        $pokemon['Generation'],
        $pokemon['Legendary'],
        $pokemon['Image'],
    );
   $pokemons->add_pokemon($pokemon);
}

