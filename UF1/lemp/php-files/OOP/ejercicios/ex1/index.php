<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pokemons</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style>
        .container {
            margin-top: 20px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1 class="text-center">Pokemons</h1>
        
        <form method="POST" action="index.php" class="mb-4">
            <div class="form-group">
                <label for="generation">Generation:</label>
                <select id="generation" name="generation" class="form-control">
                    <option value="All">All</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
            </div>

            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="type">Type:</label>
                <input type="text" id="type" name="type" class="form-control">
            </div>

            <div class="form-group">
                <label for="code">Code:</label>
                <input type="text" id="code" name="code" class="form-control">
            </div>

            <div class="form-group">
                <label for="legendary">Legendary:</label>
                <select id="legendary" name="legendary" class="form-control">
                    <option value="All">All</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </div>
            
            <button type="submit" class="btn btn-primary">Search</button>
        </form>

        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Type 1</th>
                    <th>Type 2</th>
                    <th>Health Points</th>
                    <th>Attack</th>
                    <th>Defense</th>
                    <th>Special Attack</th>
                    <th>Special Defense</th>
                    <th>Speed</th>
                    <th>Generation</th>
                    <th>Legendary</th>
                    <th>Image</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    include_once("app.php");

                    if (isset($pokemons)) {
                        $filteredPokemons = $pokemons->getPokemons();

                        // Filtrar por generación
                        if (isset($_POST['generation']) && $_POST['generation'] !== 'All') {
                            $filteredPokemons = $pokemons->get_generation($_POST['generation']);
                        }

                        // Filtrar por tipo
                        if (isset($_POST['type']) && $_POST['type'] !== '') {
                            $filteredPokemons = array_intersect($filteredPokemons, $pokemons->get_pokemon_type($_POST['type']));
                        }

                        // Filtrar por nombre
                        if (isset($_POST['name']) && $_POST['name'] !== '') {
                            $filteredPokemons = array_intersect($filteredPokemons, $pokemons->get_pokemon_name($_POST['name']));
                        }

                        // Filtrar por código
                        if (isset($_POST['code']) && $_POST['code'] !== '') {
                            $filteredPokemons = array_intersect($filteredPokemons, $pokemons->get_code($_POST['code']));
                        }

                        // Filtrar por legendario
                        if (isset($_POST['legendary']) && $_POST['legendary'] !== 'All') {
                            $legendaryValue = ($_POST['legendary'] === 'Yes');
                            $filteredPokemons = array_filter($filteredPokemons, function($pokemon) use ($legendaryValue) {
                                return $pokemon->getLegendary() === $legendaryValue;
                            });
                        }

                        // Mostrar los Pokémon filtrados
                        foreach ($filteredPokemons as $pokemon) {
                            echo "<tr>";
                            echo "<td>{$pokemon->getCode()}</td>";
                            echo "<td>{$pokemon->getName()}</td>";
                            echo "<td>{$pokemon->getType1()}</td>";
                            echo "<td>{$pokemon->getType2()}</td>";
                            echo "<td>{$pokemon->getHealthPoints()}</td>";
                            echo "<td>{$pokemon->getAttack()}</td>";
                            echo "<td>{$pokemon->getDefense()}</td>";
                            echo "<td>{$pokemon->getSpecialAttack()}</td>";
                            echo "<td>{$pokemon->getSpecialDefense()}</td>";
                            echo "<td>{$pokemon->getSpeed()}</td>";
                            echo "<td>{$pokemon->getGeneration()}</td>";
                            echo "<td>" . ($pokemon->getLegendary() ? 'Sí' : 'No') . "</td>";
                            echo "<td><img src='{$pokemon->getImage()}' alt='{$pokemon->getName()}' style='width:100px; height:100px;'></td>";
                            echo "<td>{$pokemon->total()}</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr><td colspan='14'>Error: La variable pokemons no está definida.</td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</body>

</html>


