<?php
class Card
{
    // Properties
    private int $value;
    private int $suit;
    private string $image;
    
    // Constructor
    public function __construct($value, $suit, $image)
    {
        $this->value = $value;
        $this->suit = $suit;
        $this->image = "cards/card" . (($suit - 1) * 13 + $value) . ".gif";
    }
    
    // Getters and setters
    public function getValue()
    {
        return $this->value;
    }
    
    public function getSuit()
    {
        return $this->suit;
    }
    
    public function getImage()
    {
        return $this->image;
    }
    
    // toString
    public function __toString()
    {
        return "Card: " . $this->value . " of " . $this->suit;
    }
}
?>