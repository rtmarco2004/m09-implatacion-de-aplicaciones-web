<?php
require_once 'card.php';
class Hand
{
    // Properties
    private array $cards;
    
    // Constructor
    public function __construct()
    {
        $this->cards = [];
    }
    
    // Getters and setters
    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }
    
    // Methods
    public function getCards()
    {
        return $this->cards;
    }
    
    public function __toString()
    {
        $handString = "";
        foreach ($this->cards as $card) {
            $handString .= $card . "\n";
        }
        return $handString;
    }
}
?>