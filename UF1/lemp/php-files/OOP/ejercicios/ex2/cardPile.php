<?php
require_once 'card.php';
class CardPile
{
    private array $pile;
    
    // Constructor
    public function __construct()
    {
        $this->pile = [];
    }
    
    // Methods
    // Function to add a card to the pile
    public function addCard(Card $card)
    {
        $this->pile[] = $card;
    }
    
    // Function to take a card from the pile
    public function removeCard(Card $card) : Card
    {
        $key = array_search($card, $this->pile);
        if ($key !== false) {
            $removedCard = array_splice($this->pile, $key, 1);
            return $removedCard[0];
        } 
        return new Card(0,0,"");
    }
    
    // Getters and setters
    public function getPile()
    {
        return $this->pile;
    }
}
?>