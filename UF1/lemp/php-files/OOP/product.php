<?php

class Product {
    
    //Properties
    private int $id;
    private string $name;
    private float $price;
    private array $colors;

    //Constructor

    function __construct(int $id, string $name, float $price, array $colors){
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->colors = $colors;
    }

    //Destructor (Opcional)

    function __destruct(){
        //unset($this);
    }

    //Getter and setters (Opcional)

    function get_id(): int{
        return $this->id;
    }

    function set_id(int $id): void {
        $this->id = $id;
    }

    function get_name(): string{
        return $this->name;
    }

    function set_name(string $name): void {
        $this->name = $name;
    }

    function get_price(): float{
        return $this->price;
    }

    function set_price(float $price): void {
        $this->price = $price;
    }

    function get_colors(): array{
        return $this->colors;
    }

    function set_colors(array $colors): void {
        $this->colors = $colors;
    } 
    //Class Method


    //toString (Opcional)

    function tax(float $tax): float{
        return $this-> price * $tax / (1 + $tax);
    }

    function priceNoTax(float $tax): float {
        return $this->price /(1 + $tax);
    }

    function __toString(): string{
        $text = "";
        foreach ($this->colors as $color){
            $text .= $color;
        }
        return "Product[id=" . $this->id .",name=" . $this->name .", price=". $this->price ."]";
    }
}

$colors = ["Red", "Blue", "Green"];

$p1 = new Product(1, "Basic T-shirt", 12.55, $colors);

$p2 = new Product(2, "Long T-shirt", 18.75, ["Black"]);

echo $p1->get_id() . "<br/>";
echo $p1->get_name() . "<br/>";
echo $p1->get_price() . "<br/>";
$arraycolors = $p1->get_colors();

print_r($arraycolors);
echo "<br/>";
var_dump($arraycolors);
echo "<br/>";
foreach ($colors as $color){
    echo $color ." ";
}

?>

<?php

echo "Methods: <br/>";

echo $p1 -> priceNoTax(0.21);
echo "<br>";
echo $p1 -> tax(0.21);
echo "<br>";
echo $p1 -> __toString();
echo "<br>";
echo $p2-> __toString();
?>

<?php
echo "<br>";
echo "Product Array:" . "<br>";

$products = [];
$products[] = $p1;
$products[] = $p2;

foreach ($products as $product){
    echo $product -> __toString() . "<br>";
}