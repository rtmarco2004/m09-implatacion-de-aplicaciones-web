<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
        <label for="name" > Name: </label> <br/>
        <input id="name" type="text" name="name"> <br/>
        <label for="email" >Email: </label> <br/>
        <input id="email" type="text" name="email"> <br/>
        <input type="submit" name="execute">
    </form>

    <?php

    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_POST) {
        if(isset($_POST["execute"])){
            execute();
        }
    }

    function execute() {
        if (isset($_POST["name"]))
            echo "Name: " . validate($_POST['name']) . "<br/>";
        
        if (isset($_POST["email"]))
            echo "Email: " . validate($_POST['email']) . "<br/>";
        
        
    }
?>
</body>
</html>