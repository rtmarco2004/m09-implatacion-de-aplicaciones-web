<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Action</title>
</head>
<body>

    <?php

        function validate($data){
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        if ($_POST) {
            if(isset($_POST["execute"])){
                execute();
            }
        }

        function execute() {
            if (isset($_POST["name"]))
                echo "Name: " . validate($_POST['name']) . "<br/>";
            
            if (isset($_POST["email"]))
                echo "Email: " . validate($_POST['email']) . "<br/>";
            
            
        }
    ?>
</body>
</html>