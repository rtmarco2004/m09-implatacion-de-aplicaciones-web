<!DOCTYPE HTML>
<html>
<head>
    <style>
        .error {color: #FF0000;}
    </style>
</head>
<body>
    <?php
    // define variables and set to empty values
    $nameErr = $emailErr = $genderErr = $websiteErr = $birthdayErr = "";
    $name = $email = $gender = $comment = $website = $vehicle1 = $vehicle2 = $birthday = $favcolor = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "Name is required";
        } else {
            $name = validate($_POST["name"]);
        }

        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
        } else {
            $email = validate($_POST["email"]);
        }

        if (empty($_POST["website"])) {
            $website = "";
        } else {
            $website = validate($_POST["website"]);
        }

        if (empty($_POST["comment"])) {
            $comment = "";
        } else {
            $comment = validate($_POST["comment"]);
        }

        if (empty($_POST["gender"])) {
            $genderErr = "Gender is required";
        } else {
            $gender = validate($_POST["gender"]);
        }
        $vehicle1 = validate($_POST["vehicle1"]);
        $vehicle2 = validate($_POST["vehicle2"]);

        if (empty($_POST["birthday"])) {
            $birthdayErr = "Birthday is required";
        } else {
            $birthday = validate($_POST["birthday"]);
        }
         $favcolor = validate($_POST["favcolor"]);
    }

    function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>

    <h2>PHP Form Validation Example</h2>
    <p>
        <span class="error">* required field</span>
    </p>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Name: <input type="text" name="name" />
        <span class="error">
            * <?php echo $nameErr;?>
        </span>

        <br />
        <br />
        E-mail: <input type="text" name="email" />
        <span class="error">
            * <?php echo $emailErr;?>
        </span>

        <br />
        <br />
        Website: <input type="text" name="website" />
        <span class="error">
            <?php echo $websiteErr;?>
        </span>

        <br />
        <br />
        Comment: <textarea name="comment" rows="5" cols="40"></textarea>
        <br />
        <br />
        Gender:
        <input type="radio" name="gender" value="female" />
        Female
        <input type="radio" name="gender" value="male" />
        Male
        <input type="radio" name="gender" value="other" />
        Other
        <span class="error">
            * <?php echo $genderErr;?>
        </span>

        <br />
        <br />
        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" />
        <label for="vehicle1"> I have a bike</label>
        <br />
        <input type="checkbox" id="vehicle2" name="vehicle2" value="Car" />
        <label for="vehicle2"> I have a car</label>
        <br />
        <br />
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday" />
        <span class="error">
            * <?php echo $birthdayErr; ?>
        </span>

        <br />
        <br />
        <label for="favcolor">Select your favorite color:</label>
        <input type="color" id="favcolor" name="favcolor" value="#ff0000" />
        <br />
        <br />
        <input type="submit" name="submit" value="Submit" />
    </form>

    <?php
    echo "<h3>Your Input:</h3>";
    echo $name;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $website;
    echo "<br>";
    echo $comment;
    echo "<br>";
    echo $gender;
    echo "<br>";
    echo $vehicle1;
    echo "<br>";
    echo $vehicle2;
    echo "<br>";
    echo $birthday;
    echo "<br>";
    echo $favcolor;
    ?>

</body>
</html>