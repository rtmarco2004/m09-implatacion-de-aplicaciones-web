<?php
// declare(strict_types=1); // strict requirement
$x = 5;
$y = 10;

function add( float $x1 = 10, float $x2 = 10) : float
{
    return $x1 + $x2;
}

echo "Add: " . add() . "<br />";

function mult( float $x1 = 10, float $x2 = 10) : float
{
    return $x1 * $x2;
}

$a = 10;
$b = 15;

echo "Mult: " . mult($a, $b) . "<br />";

echo "a: " . $a . "<br />";
echo "b: " . $b . "<br />";
?>