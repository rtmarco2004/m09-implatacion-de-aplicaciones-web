<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
    $h1 = "Title Page";
    $txt = "<h1>" . $h1 . "</h1>"; 
    $txt .= "<h2>Header h2</h2>";
    // .= para concatenar.
    // tambien sirve $txt = "<h1>$h1</h1>";
    echo $txt;

    $p = "This is a very silly line with a \$txt";
    echo "<p>$p</p>";

    function myfunction1() {
        //global $div;
        $div = "This is a div";
        echo "<div>$div</div>";
    }

    myfunction1();

    echo "<br /> GLOBAL:<br/>";
    echo $GLOBALS['h1'];
    echo $GLOBALS['txt'];
    //echo $GLOBALS['div'];

    function myfunction2(){
        static $i = 0;
        echo $i . "<br />";
        $i++;
    }

    function myfunction2_1(){
        $i = 0;
        echo $i;
        $i++;
    }

    echo "<br />";
    myfunction2();
    myfunction2();
    myfunction2();

    myfunction2_1();
    myfunction2_1();
    myfunction2_1();

    echo "<br />" . "<br />";

    if(isset($h1)) {
        echo "\$h1 exists" . "<br />";
    }   else {
        echo "\$h1 does not exist" . "<br />";
    }


    $y= 100;
    $f = 20.56;
    $t = "Text";
    $b = True;

    $result = sprintf("This is the result: %d, %f, %s, %b", $y, $f, $t, $b);

    echo " <br />" . $result . "<br />";

    echo "{$y} {$b}" . "<br />";

    $lorem = "Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas , las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.";

    echo strlen($lorem) . "<br />";
    echo str_word_count($lorem) . "<br />";
    echo strrev($lorem) . "<br />";
    echo strpos($lorem, "simplemente") . "<br />";

    $z = 120;

    echo PHP_INT_MAX . "<br />";

    $z2 = $z * 2.90;
    echo $z2 . "<br />";

    echo $z * 2.89 . "<br />";

    echo is_int($z) . "<br />";
    echo is_int($z2) . "<br />";

    $z3 = "126";

    echo intval($z3) . "<br />";

    echo  PHP_FLOAT_MAX . "<br />";

    echo var_dump($z3) . "<br />";

    echo var_dump($z2) . "<br />";

    echo var_dump($b) . "<br />";

    echo var_dump($f) . "<br />";

    $asin = asin(100);

    if (!is_nan($asin)){
        echo $asin . "<br />";
        echo var_dump($asin) . "<br />";
    }

    $bol1 = TRUE;
    $bol2 = FALSE;

    echo "bol1 = $bol1" . "<br />";
    echo "bol2 = $bol2" . "<br />";

    var_dump($bol1) . "<br />";
    var_dump($bol2) . "<br />"; 

    if ($bol1) {
        echo "bol1 is true" . "<br />";
    }else {
        echo "bol1 is false" . "<br />";
    }


    $a = 10;
    $b = 4;

    $pow = $a ** $b;

    echo "pow = $pow" . "<br />";

    $c = 120;
    $d = 40;

    $res = $a <=> $b;
    echo $res . "<br />";

    $res2 = $a == $b ? ($c > $d ? "1" : "2") : "3";
    echo $res2 . "<br />";
?>
</body>
</html>