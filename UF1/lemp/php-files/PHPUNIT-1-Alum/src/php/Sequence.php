<?php
class Sequence
{

  /*
7. Sequence

Aquest mètode retorna una seqüència de nombres de $n elements.
El paràmetre $array són els 3 primers números de la seqüència.
El valor de la posició i és la suma de i-1 i i-3.

Exemples:

sequence([1,1,1],10); 	// return [1,1,1,2,3,4,6,9,13,19]
sequence([0,0,1],10); 	// return [0,0,1,1,1,2,3,4,6,9]
sequence([3,2,1],10); 	// return [3,2,1,4,6,7,11,17,24,35]
sequence([1,1,1],1); 	// return [1]
sequence([1,2,3],0); 	// return []

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\SequenceTest.php


  */

  public function sequence(array $array, int $n): array
  {
    //TODO
    return [];
  }
}
