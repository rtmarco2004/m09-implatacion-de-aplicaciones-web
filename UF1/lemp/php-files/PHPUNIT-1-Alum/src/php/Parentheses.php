<?php
class Parentheses
{

  /*
9. Parentheses
Escriu una funció que pren un string de parèntesis, i determina si l'ordre de parèntesis és vàlid. La funció ha de retornar true si el string és vàlid, i false si és invàlid.

Examples:

parentheses("()");              // return true
parentheses("(())()");          // return true
parentheses("(");               // return false
parentheses("(())((()())())");  // return true
parentheses("()())())");	 // return false

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\ParenthesesTest.php


*/

  public function parentheses($parenStr): bool
  {
    //TODO
    return false;
  }
}
