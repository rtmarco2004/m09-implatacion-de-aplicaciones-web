<?php
class Divisors
{

  /*
6. Divisors

Aquesta funció pren com a paràmetre un nombre > 1 ($integer) i retorna un array amb tots els enters divisors de $integer (excepte per 1 i el mateix nombre $integer).
Mostra els valors de més gran a més petit.

Si el nombre és un nombre primer (prime) retorna un string '(integer) is prime'.

Examples:

divisors(12); 	// return [6, 4, 3, 2]
divisors(100); 	// return [50, 25, 20, 10, 5, 4, 2]
divisors(13); // => '13 is prime'

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\DivisorsTest.php


*/

  public function divisors(int $integer): array | string
  {
    //TODO
    return "";
  }
}
