<?php

class Multiples
{

  /*

  2. Multiples of N Array

Retorna un array amb tots els els nombres múltiples d’un número passat per argument.

Per exemple per, si $numbers és [5, 100, 25, 20, 4, 3, 10], i $mult és 5, retornarà el següent array:

[5, 100, 25, 20, 10]

Si $numbers éstà buit, retorna un array buit.
Si $mult és 0, retorna "Multiple can not be 0".

Exemples:

multiples([5, 100, 25, 20, 4, 3, 10], 5);			// return [5, 100, 25, 20, 10]
multiples([-3, 100, -150, 20, 6, 37, 300, 10, 27], 3);	// return [-3, -150, 6, 300, 27]

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\MultiplesTest.php

*/

public function multiples(array $numbers, int $mult): array | string
  {
    if ($mult === 0) {
      return "Multiple can not be 0";
    }

    $result = [];
    foreach ($numbers as $number) {
      if ($number % $mult === 0) {
        $result[] = $number;
      }
    }

    return $result;
  }
}
