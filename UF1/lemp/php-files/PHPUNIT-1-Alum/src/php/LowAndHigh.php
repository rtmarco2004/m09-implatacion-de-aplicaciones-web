<?php
class lowAndHigh
{

    /*
3. Low and high

A partir d’un string que conté nombres separats per espais, hem de retornar un array amb el nombre més petit i el nombre més elevat. 

Notes:
Tots els nombres són vàlids, no cal validar-los.
El nombre més petit és el primer element de l'array.
L'array conté enters.

Exemples:

lowAndHigh("1 2 3 4 5");  // return [1, 5]
lowAndHigh("1 2 -3 4 5"); // return [-3, 5]
lowAndHigh("1 9 3 4 -5"); // return [-5, 9]

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\LowAndHighTest.php

*/

    public function lowAndHigh(string $numbers): array
{
    }
