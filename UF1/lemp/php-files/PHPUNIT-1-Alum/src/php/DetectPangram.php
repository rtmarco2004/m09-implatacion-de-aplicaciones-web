<?php
class DetectPangram
{

  /*
5. Detect pangram


Detecte si un string conté totes les lletres de l'abecedari (no importa si són majúscules o minúscules).

Es considera com a lletres de l'abecedari, les següents lletres:
abcdefghijklmnopqrstuvwxyz

Exemples:

detectPangram("The quick brown fox jumps over the lazy dog."); 	// retrun true
detectPangram("1L%r+f4G!e7w V z q6M h4d F3b+t O2n e K^g+c#S^i4i X7c-u P5d7j Y6a(a B"); // return true
detectPangram("A pangram is a sentence that contains every single letter of the alphabet at least once.");  // return false

Executar proves:
Obrir terminal.
Siturar-se al directori "tests".
Executar el test unitari: 
php phpunit.phar --testdox .\unit\DetectPangramTest.php

*/

  public function detectPangram(string $string): bool
  {
    //TODO
    return false;
  }
}
