# PHP Activitats

## 1. Factorial

```php
public function factorial(int $num): int | string
	//TODO
}
```

La fórmula per calcular el factorial és:

```
n! = n × (n-1) × (n-2) × … × 1
```
 
El factorial de zero és 1: 

```
0! = 1
```

Exemples:

```php
factorial(5) = 120
factorial(7) = 5040
factorial(0) = 1
factorial(3) = 6
factorial(-7) = "$num can not be negative"
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari:

```
php phpunit.phar --testdox .\unit\FactorialTest.php
```
## 2. Multiples of 5 or 7

```php
function multiples5and7(int $number): int{
	//TODO
}
```

Suma tots els els nombres naturals múltiples de 5 o 7 que hi ha entre 0 i un número passat per argument.
Per exemple per, si l’argument és 10, el resultat serà 5 + 7 + 10 = 22.

Si l’argument és negatiu o zero, retorna 0.

Nota: si el nombre és múltiple de 5 i 7 a la vegada, només el sumarà un cop.

Exemples:
```php
multiples5and7(10); 	// return 22
multiples5and7(100); 	// return 1680
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\Multiples5and7Test.php
```

## 3. Multiples of N Array

```php
function multiples(array $numbers, int $mult): array | string {
	//TODO
}
```

Retorna un array amb tots els els nombres múltiples d’un número passat per argument.

Per exemple per, si $numbers és [5, 100, 25, 20, 4, 3, 10], i $mult és 5, retornarà el següent array:

```
[5, 100, 25, 20, 10]
```

* Si $numbers éstà buit, retorna un array buit.
* Si $mult és 0, retorna "Multiple can not be 0".

Exemples:

```
multiples([5, 100, 25, 20, 4, 3, 10], 5);			// return [5, 100, 25, 20, 10]
multiples([-3, 100, -150, 20, 6, 37, 300, 10, 27], 3);	// return [-3, -150, 6, 300, 27]
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\MultiplesTest.php
```

## 4. Low and high

```php
function lowAndHigh(string $numbers) : array {
	//TODO
}
```

A partir d’un string que conté nombres separats per espais, hem de retornar un array amb el nombre més petit i el nombre més elevat. 

Notes:

* Tots els nombres són vàlids, no cal validar-los.
* El nombre més petit és el primer element de l'array.
* L'array conté enters.

Exemples:

```php
lowAndHigh("1 2 3 4 5");  // return [1, 5]
lowAndHigh("1 2 -3 4 5"); // return [-3, 5]
lowAndHigh("1 9 3 4 -5"); // return [-5, 9]
```
Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\LowAndHighTest.php
```

## 5. Detect vowels

```php
function detectVowels(string $string): bool { 
	//TODO
}
```

Detecte si un string conté totes les vocals (no importa si són majúscules o minúscules).

Tots els strings es consideren sense accents.

Exemples:

```php
detectVowels("OUbacity e hhy"); 	// retrun true
detectVowels("e412e 6obnh i jk u"); 	// return false
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\DetectVowelsTest.php
```

## 6. Detect pangram

```php
function detectPangram(string $string): bool { 
	//TODO
}
```

Detecte si un string conté totes les lletres de l'abecedari (no importa si són majúscules o minúscules).

Es considera com a lletres de l'abecedari, les següents lletres:

```
abcdefghijklmnopqrstuvwxyz
```

Exemples:

```php
detectPangram("The quick brown fox jumps over the lazy dog."); 	// retrun true
detectPangram("1L%r+f4G!e7w V z q6M h4d F3b+t O2n e K^g+c#S^i4i X7c-u P5d7j Y6a(a B"); // return true
detectPangram("A pangram is a sentence that contains every single letter of the alphabet at least once.");  // return false
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\DetectPangramTest.php
```

## 7. Divisors

```php
function divisors(int $integer) : array | string {
	//TODO
}
```

Aquesta funció pren com a paràmetre un nombre > 1 ($integer) i retorna un array amb tots els enters divisors de $integer (excepte per 1 i el mateix nombre $integer).

Mostra els valors de més gran a més petit.

Si el nombre és un nombre primer (prime) retorna un string '(integer) is prime'.

Examples:

```php
divisors(12); 	// return [6, 4, 3, 2]
divisors(100); 	// return [50, 25, 20, 10, 5, 4, 2]
divisors(13); // => '13 is prime'
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\DivisorsTest.php
```


## 8. Sequence

```php
function sequence(array $array, int $n): array {
	//TODO
}
```

Aquest mètode retorna una seqüència de nombres de $n elements.

El paràmetre $array són els 3 primers números de la seqüència.

El valor de la posició i és la suma de i-1 i i-3.

Exemples:

```php
sequence([1,1,1],10); 	// return [1,1,1,2,3,4,6,9,13,19]
sequence([0,0,1],10); 	// return [0,0,1,1,1,2,3,4,6,9]
sequence([3,2,1],10); 	// return [3,2,1,4,6,7,11,17,24,35]
sequence([1,1,1],1); 	// return [1]
sequence([1,2,3],0); 	// return []
```

Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\SequenceTest.php
```


## 9. Binary

```php
function binary2Number (array $arr): int {
	//TODO
}
```

Donat un array ($arr) que conté el nombre en binari, retornar el número en decimal.

Si $arr està buida, el mètode retorna un -1.

Exemples:

```php
binary2Number([0,0,1,0]); 		// return 2
binary2Number([0,1,1,0,0,1,1,1]); 	// return 15
binary2Number([1,1,1,1,1,1,1,1]); 	// return 256
binary2Number([1,0,0,0,0,0,0,0]); 	// return 128
binary2Number([]); 			// return -1
```


Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\BinaryTest.php
```

## 10. Parentheses

```php
function parentheses(string $parenStr): bool {
	//TODO
}
```

Escriu una funció que pren un string de parèntesis, i determina si l'ordre de parèntesis és vàlid. La funció ha de retornar true si el string és vàlid, i false si és invàlid.

Examples:

```php
parentheses("()");              // return true
parentheses("(())()");          // return true
parentheses("(");               // return false
parentheses("(())((()())())");  // return true
parentheses("()())())");	 // return false
```


Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\ParenthesesTest.php
```

## 11. MoveZeros

```php
function parentheses(string $parenStr): bool {
	//TODO
}
```

Aquesta funció mou tots els zeros al final preservant l'ordre dels elements.
  
Exemples:

```php
moveZeros([false,1,0,1,2,0.0,1,3,"a",null,[],0) // returns[false,1,1,2,1,3,"a",null,[],0,0,0]
moveZeros([false,0,1,3,"a",null,[],[1,2,3],0])	  // returns [false,1,3,"a",null,[],[1,2,3],0,0]
Nota: els 0.0 es transformen en 0.
```


Executar proves:

* Obrir terminal.
* Siturar-se al directori "tests".
* Executar el test unitari: 

```
php phpunit.phar --testdox .\unit\MoveZeros.php
```