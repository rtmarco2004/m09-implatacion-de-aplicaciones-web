<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class DivisorsTest extends TestCase {

  #[Test]
  #[TestDox("Divisors")]
  public function divisors(): void {

    require_once __DIR__ . "/../../src/php/Divisors.php";

    $div = new Divisors();


    $this->assertSame([5, 3],  $div->divisors(15));
    $this->assertSame([6, 4, 3, 2], $div->divisors(12));
    $this->assertSame([50, 25, 20, 10, 5, 4, 2], $div->divisors(100));
    $this->assertSame('13 is prime', $div->divisors(13));
    $this->assertSame('17 is prime', $div->divisors(17));
  }

}
    
