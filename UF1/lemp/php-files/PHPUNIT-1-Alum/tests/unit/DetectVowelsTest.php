<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class DetectVowelsTest extends TestCase {


    #[Test]
    #[TestDox("Detect vowels")]
    public function testDetectVowels(): void {

        require_once __DIR__ . "/../../src/php/DetectVowels.php";

        $dw = new DetectVowels();

        $this->assertTrue( $dw->detectVowels("aeIoU"));
        $this->assertTrue($dw->detectVowels("OUbacity e hhy"));
        $this->assertTrue($dw->detectVowels("e4a12e 6obnh i jk u"));
        $this->assertFalse($dw->detectVowels("e412e 6obnh i jk u"));
    }


}
    
