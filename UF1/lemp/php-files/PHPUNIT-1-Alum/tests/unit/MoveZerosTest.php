<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class MoveZerosTest extends TestCase {

   
//8. Binary
#[Test]
#[TestDox("Facotrial")]
public function testFactorial() {

    require_once __DIR__ . "/../../src/php/MoveZeros.php";

    $mov = new MoveZeros();

    $this->assertSame([false,1,1,2,1,3,"a",null,[],[1,2,3],[0,1,0.0],0,0,0], $mov->moveZeros([false,1,0,1,2,0.0,1,3,"a",null,[],[1,2,3],[0,1,0.0],0]));
    $this->assertSame([true,1,2,1,1,3,1,0,0,0,0], $mov->moveZeros([true,1,2,0,1,0,1,0,3,0,1]));
    
    $this->assertSame([1,2,1,1,3,1,0,0,0,0], $mov->moveZeros([1,2,0,1,0,1,0,3,0,1]));
        $this->assertSame([9,9,1,2,1,1,3,1,9,9,0,0,0,0,0,0,0,0,0,0], $mov->moveZeros([9,0.0,0,9,1,2,0,1,0,1,0.0,3,0,1,9,0,0,0,0,9]));
        $this->assertSame(["a","b","c","d",1,1,3,1,9,9,0,0,0,0,0,0,0,0,0,0], $mov->moveZeros(["a",0,0,"b","c","d",0,1,0,1,0,3,0,1,9,0,0,0,0,9]));
        $this->assertSame(["a","b",null,"c","d",1,false,1,3,[],1,9,9,0,0,0,0,0,0,0,0,0,0], $mov->moveZeros(["a",0,0,"b",null,"c","d",0,1,false,0,1,0,3,[],0,1,9,0,0,0,0,9]));
        $this->assertSame([1,null,2,false,1,0,0], $mov->moveZeros([0,1,null,2,false,1,0]));
        $this->assertSame(["a","b"], $mov->moveZeros(["a","b"]));
        $this->assertSame(["a"], $mov->moveZeros(["a"]));
        $this->assertSame([0,0], $mov->moveZeros([0,0]));
        $this->assertSame([0], $mov->moveZeros([0]));
        $this->assertSame([false], $mov->moveZeros([false]));
        $this->assertSame([], $mov->moveZeros([]));

  }
}
    
