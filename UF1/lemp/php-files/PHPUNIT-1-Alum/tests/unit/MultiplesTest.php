<?php 
declare(strict_types=1);


use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class MultiplesTest extends TestCase {

    
    #[Test]
    #[TestDox("Test Array Multiples")]
    public function testMultiples(): void {

        require_once __DIR__ . "/../../src/php/Multiples.php";

        $mult = new Multiples();

        $this->assertSame([5, 100, 25, 20, 10], $mult->multiples([5, 100, 25, 20, 4, 3, 10], 5));
        $this->assertSame([-3, -150, 6, 300, 27], $mult->multiples([-3, 100, -150, 20, 6, 37, 300, 10, 27], 3));
        $this->assertSame([-3, -150, 6, 300, 27], $mult->multiples([-3, 100, -150, 20, 6, 37, 300, 10, 27], -3));
        $this->assertSame([0, 4, 120, 50, 4], $mult->multiples([-3, 0, -15, 4, 120, 61, 37, 50, 101, 4, 1], 2));
        $this->assertSame([], $mult->multiples([], 5));
        $this->assertSame("Multiple can not be 0", $mult->multiples([-3, 0, -15, 4, 120, 61, 37, 50, 101, 4, 1], 0));
       
    }
  
   
}
    
